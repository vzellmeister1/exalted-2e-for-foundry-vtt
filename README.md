# Exalted 2E for Foundry VTT

Implementation of Exalted 2E for [Foundry VTT](https://foundryvtt.com/). Guides on how to implement your own game system for the platform can be found [here](https://foundryvtt.com/article/system-development/).